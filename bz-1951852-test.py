import io
import sys
import ruamel.yaml
from ruamel.yaml import YAML

ruamel_yaml = YAML(typ="rt")
ruamel_yaml.default_flow_style = False
ruamel_yaml.preserve_quotes = True
ruamel_yaml.width = 1024

buf = """\
# top level
top:
  # next level 1
  - { here: 1, there: 2 }
  # next level 2
  - { there: 1, here: 2 }
top2:
  # next level 1-2
  - { here: 11, there: 22 }
  # next level 2-2
  - { there: 11, here: 22 }
"""

if len(sys.argv) > 1:
    buf = open(sys.argv[1]).read()
ruamel_data = ruamel_yaml.load(buf)
ruamel_yaml.indent(mapping=2, sequence=4, offset=2)
outf = io.StringIO()
ruamel_yaml.dump(ruamel_data, outf)
buf = outf.getvalue()
print(buf)
ruamel_data = ruamel_yaml.load(buf)
outf = io.StringIO()
ruamel_yaml.dump(ruamel_data, outf)
buf = outf.getvalue()
print(buf)
